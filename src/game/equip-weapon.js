export default function EquipWeapon({active, onClick}) {
  if (active === true) {
    return (
      <button className="button" onClick={onClick}>équipper</button>
    );
  }

  return (
    <button className="button" disabled={true}>équippé</button>
  );
}

