import {labelForClasse} from './classe';
import EquipWeapon from './equip-weapon';
import Player from './player';
import Weapon from "./weapon";

interface PlayerComponentProps {
  player: Player, 
  onSelectWeapon: (weapon: Weapon) => void 
}

function PlayerComponent({ player, onSelectWeapon}: PlayerComponentProps) {
  const { name, classe, equippedWeapon, bag } = player;
  return (
    <div>
      {/* tag::content[] */}
      <h1>{name}</h1> 
      <h3 data-testid="player-class">{labelForClasse(classe)}</h3>
      <hr/>
      <ul>
        {bag.map((weapon) => ( 
            <li key={weapon.id} data-testid={`bag-weapon-${weapon.id}`}>
              <EquipWeapon
                active={equippedWeapon === weapon}
                onClick={() => onSelectWeapon(weapon)}
              />
              ({weapon.requiredClass}) {weapon.name} <em>{weapon.description}</em>
            </li>
          )
        )}
      </ul>
      {/* end::content[] */}
    </div>
  );
}

export default PlayerComponent;