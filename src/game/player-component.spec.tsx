import { render, screen } from "@testing-library/react";
import renderer from "react-test-renderer";
import Classe from "./classe";
import Player from "./player";
import PlayerComponent from "./player-component";
import Weapon from "./weapon";

interface WeaponFactory {
  createDagger(): Weapon;
  createSword(): Weapon;
}
const weaponFactory: WeaponFactory = {
  createSword() {
    return new Weapon(
      "1a8d",
      "Epée courte",
      Classe.WARRIOR,
      "Maniable et équilibrée."
    );
  },
  createDagger() {
    return new Weapon(
      "f52e",
      "Dague rituelle",
      Classe.ROGUE,
      "Une courte dague effilée"
    );
  },
};

// tag::snapshot[]
test("match snapshot", () => {
  /* Arrange */
  const player: Player = new Player("kevinDu67", Classe.WARRIOR, [
    weaponFactory.createSword(),
  ]);

  /* Act */
  const result = renderer
    .create(<PlayerComponent player={player} onSelectWeapon={() => {}} />)
    .toJSON();

  /* Assert */
  expect(result).toMatchSnapshot();
});
// end::snapshot[]

// tag::data_attribute[]
test("affiche la classe du joueur", () => {
  /* Arrange */
  const player = new Player("kevinDu67", Classe.WARRIOR, [
    weaponFactory.createSword(),
  ]);

  /* Act */
  render(<PlayerComponent player={player} onSelectWeapon={() => {}} />);

  /* Assert */
  expect(screen.getByTestId("player-class").innerHTML).toEqual(
    expect.stringContaining("Guerrier")
  );
});
// end::data_attribute[]

test("affiche l'inventaire", () => {
  // Arrange
  const sword = weaponFactory.createSword();
  const dagger = weaponFactory.createDagger();
  const player = new Player("kevinDu67", Classe.WARRIOR, [sword, dagger]);

  // Act
  render(<PlayerComponent player={player} onSelectWeapon={() => {}} />);
  const items = screen.getAllByTestId(/bag-weapon-.*/);

  // Assert
  expect(items.length).toEqual(2);

  const ids = items
    .map((it) => it.attributes)
    .map((attrs) => attrs.getNamedItem("data-testid"))
    .filter((it) => it !== null)
    .map((attr) => attr?.value ?? "")
    .map((testId) => testId.replace("bag-weapon-", ""));
  expect(ids).toEqual(["1a8d", "f52e"]);
});
