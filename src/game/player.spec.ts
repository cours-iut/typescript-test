import Classe from "./classe";
import Player from "./player";
import Weapon from "./weapon";

function kevinDu67() {
    return new Player(
        "kevinDu67",
        Classe.WARRIOR
    )
}

function sword(): Weapon {
    return new Weapon(
        "1a8d",
        "Epée",
        Classe.WARRIOR
    )
}

function staff(): Weapon {
    return new Weapon(
        "2ef4",
        "Baton",
        Classe.MAGE
    )
}

test("can equip a warrior weapon", () => {
  // Arrange
  const player = kevinDu67();

  // Act
  const canEquip = player.canEquip(sword())

  // Assert
  expect(canEquip).toBe(true)
});

test("a warrior cannot equip a mage weapon", () => {
    // Arrange
    const kevin = kevinDu67();

    // Act
    const canEquip = kevin.canEquip(staff());

    // Assert
    expect(canEquip).toBe(false);
})
