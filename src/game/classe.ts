enum Classe {
  // Available classes
  WARRIOR = 1,
  MAGE = 2,
  ROGUE = 3,
  DRUID = 4,
}

interface LabelMap {
  [key: number]: string;
}
const labels: LabelMap = {};

labels[Classe.WARRIOR] = "Guerrier";
labels[Classe.MAGE] = "Mage";
labels[Classe.ROGUE] = "Voleur";
labels[Classe.DRUID] = "Druide";

function labelForClasse(classe: Classe): string {
  return labels[classe];
}

export default Classe;
export { labelForClasse };
