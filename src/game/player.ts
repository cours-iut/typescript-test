import Classe from "./classe";
import Weapon from "./weapon";

class Player {
  constructor(
    public name: string,
    public classe: Classe,
    public bag: Weapon[] = [],
    public equippedWeapon?: Weapon
  ) {}

  canEquip(weapon: Weapon) {
    if (this.classe === Classe.WARRIOR || this.classe === Classe.ROGUE) {
      return (
        weapon.requiredClass === Classe.WARRIOR ||
        weapon.requiredClass === Classe.ROGUE
      );
    }
    return weapon.requiredClass === this.classe;
  }

}

export default Player;
