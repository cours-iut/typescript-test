import Classe from "./classe";

class Weapon {
  constructor(
    public id: string,
    public name: string,
    public requiredClass: Classe,
    public description: string = ""
  ) {}
}

export default Weapon;
