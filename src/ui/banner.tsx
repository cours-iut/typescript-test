export default function Banner({ children }) {
  return <h1 data-testid="title">{children}</h1>;
}
