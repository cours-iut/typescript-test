with (import <nixpkgs> {});

mkShell {
    buildInputs = [
        yarn
        nodejs-16_x
    ];

    shellHook = ''
      yarn config set prefix $PWD/.yarn
      export PATH="$(yarn global bin):$PATH"
    '';
}
